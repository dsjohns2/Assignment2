from bs4 import BeautifulSoup
import requests
import networkx as nx
import matplotlib.pyplot as plt
import logging
import json
import heapq
import scipy
import numpy

class Graph:
    """
    This is the class made up of actor and movie nodes.
    """

    def __init__(self):
        """
        This is the constructor of the graph class.
        """
        self.explored_nodes = []
        self.num_actor_nodes = 0
        self.num_movies_nodes = 0

    def attempt_to_add_node(self, node):
        """
        This function checks if node is already in the graph and adds it if it is not.
        :param node: the Node to add
        :return: a list of new Nodes to explore
        """
        # Search the explored nodes list to see if node is in the list
        is_new_node = True
        for explored in self.explored_nodes:
            if (explored.name == node.name):
                is_new_node = False
        # Decide whether to add the node to the graph
        if (is_new_node):
            # Add the node and expand the node if it is not in the graph
            self.explored_nodes.append(node)
            nodes_to_explore = node.expand_node()
            if (node.node_type == "Movie"):
                self.num_movies_nodes += 1
            else:
                self.num_actor_nodes += 1
            logging.info(node.name + " successfully added.")
        else:
            # Don't add the node to the graph
            nodes_to_explore = []
            logging.info(node.name + " not added because it is a repeat.")
        return nodes_to_explore

    def generate_edges(self):
        """
        The neighbors of a node determine the edges in a graph.
        This function adds an edge between each node and the parent that found it.
        """
        for node in self.explored_nodes:
            if (node.parent != None):
                node.neighbors.append(node.parent)
                node.parent.neighbors.append(node)

    def generate_visual_graph(self, actor_num, movie_num):
        """
        This function visualizes the graph.
        :param actor_num number of actor nodes to display
        :param movie_num number of movie nodes to display
        """
        G = nx.Graph()
        nodes_in_graph = []
        cur_actor_num = 0
        cur_movie_num = 0
        i = 0
        while(cur_actor_num < actor_num or cur_movie_num < movie_num):
            if(self.explored_nodes[i].node_type == "Actor" and cur_actor_num < actor_num):
                    nodes_in_graph.append(self.explored_nodes[i])
                    cur_actor_num += 1
            if(self.explored_nodes[i].node_type == "Movie" and cur_movie_num < movie_num):
                    nodes_in_graph.append(self.explored_nodes[i])
                    cur_movie_num += 1
            i += 1
        for node in nodes_in_graph:
            G.add_node(node.name)
        for node in nodes_in_graph:
            for neighbor in node.neighbors:
                if(neighbor in nodes_in_graph):
                    G.add_edge(node.name, neighbor.name)

        nx.draw(G, with_labels=True)
        plt.show()

    def write_to_json(self):
        """
        This function writes the current graph structure into a JSON file: scraper.json.
        """
        master_string = ""
        for node in self.explored_nodes:
            node_string = node.name + ";" + node.node_type + ";" + node.link + ";" + str(
                node.value) + ";" + "Neighbors:,"
            for neighbor in node.neighbors:
                node_string += neighbor.name + ","
            master_string += node_string + "\n"
        data = json.dumps(master_string)
        with open("scraper.json", "w") as fp:
            fp.write(data)

    def read_from_json(self, file):
        """
        This function generates a graph object based off of the current data in file.
        :param file: the file to read data from
        """
        self.explored_nodes = []
        with open(file) as json_file:
            data = json.load(json_file)
        node_strs = data.split("\n")[:-1]
        for node_str in node_strs:
            node_elems = node_str.split(";")
            name = node_elems[0]
            node_type = node_elems[1]
            link = node_elems[2]
            value = int(node_elems[3])
            neighbors = node_elems[4].split(",")[1:-1]
            new_node = Node(node_type, name, link, None, neighbors, value)
            self.explored_nodes.append(new_node)
        for node in self.explored_nodes:
            new_neighbors_list = []
            for test_neighbor in node.neighbors:
                for test_node in self.explored_nodes:
                    if (test_neighbor == test_node.name):
                        new_neighbors_list.append(test_node)
            node.neighbors = new_neighbors_list

    def read_from_given_json(self, file):
        """
        This function generates a graph object based off of the current data in class given json file.
        :param file: the file to read data from
        """
        self.explored_nodes = []
        with open(file) as json_file:
            data = json.load(json_file)[0]
            # Add all actor nodes
            for elem in data:
                elem_dict = data[elem]
                name = elem_dict["name"]
                node_type = elem_dict["json_class"]
                link = ""
                value = elem_dict["age"]
                neighbors = elem_dict["movies"]
                new_node = Node(node_type, name, link, None, neighbors, value)
                new_node.actor_gross = elem_dict["total_gross"]
                self.explored_nodes.append(new_node)
            # Add movie nodes and neighbors
            movies_list = []
            for actor_node in self.explored_nodes:
                new_neighbors_list = []
                for test_neighbor in actor_node.neighbors:
                    neighbor_found = False
                    for test_node in self.explored_nodes:
                        if(test_neighbor == test_node.name):
                            # neighbor movie node found
                            new_neighbors_list.append(test_node)
                            test_node.neighbors.append(actor_node)
                            neighbor_found = True
                    if(not neighbor_found):
                        # neighbor movie node not found
                        new_node = Node("Movie", test_neighbor, "", None, [], 0)
                        new_neighbors_list.append(new_node)
                        new_node.neighbors.append(actor_node)
                        movies_list.append(new_node)
                actor_node.neighbors = new_neighbors_list
            self.explored_nodes += movies_list

    def get_movie_gross(self, movie_name):
        """
        This function prints out the gross of movie_name
        :param movie_name: name of the movie
        """
        print(movie_name + " grossed the following amount of money:")
        for node in self.explored_nodes:
            if (node.name == movie_name):
                print(node.value)
        print()

    def get_actor_movies(self, actor_name):
        """
        This function prints out the movies that actor_name is in
        :param actor_name: name of the actor
        """
        print(actor_name + " is in the following movies:")
        for node in self.explored_nodes:
            if (node.name == actor_name):
                for neighbor in node.neighbors:
                    print(neighbor.name)
        print()

    def get_movie_actors(self, movie_name):
        """
        This function prints out the actors in movie_name
        :param movie_name: name of the movie
        """
        print("These are the actors in " + movie_name + ":")
        for node in self.explored_nodes:
            if (node.name == movie_name):
                for neighbor in node.neighbors:
                    print(neighbor.name)
        print()

    def get_oldest_actors(self, x):
        """
        This function prints out the x oldest actors in the graph
        :param x: number of actors to print
        """
        print("These are the " + str(x) + " oldest actors:")
        pri_queue = []
        for node in self.explored_nodes:
            if (node.node_type == "Actor"):
                heapq.heappush(pri_queue, (-1 * node.value, node.name))
        for i in range(0, x):
            if (len(pri_queue) > 0):
                elem = heapq.heappop(pri_queue)
                print(elem[1], -1 * elem[0])
        print()

    def get_highest_grossing_movies(self, x):
        """
        This function prints out the x highest grossing movies in the graph
        :param x: number of movies to print
        """
        print("These are the " + str(x) + " highest grossing movies:")
        pri_queue = []
        for node in self.explored_nodes:
            if (node.node_type == "Movie"):
                heapq.heappush(pri_queue, (-1 * node.value, node.name))
        for i in range(0, x):
            if (len(pri_queue) > 0):
                elem = heapq.heappop(pri_queue)
                print(elem[1], -1 * elem[0])
        print()

    def generate_hub_list(self):
        """
        This function prints out the list of hub connections oldest actors in the graph
        """
        pri_queue = []
        for node in self.explored_nodes:
            if(node.node_type == "Actor"):
                connection_num = 0
                for other_actor in self.explored_nodes:
                    if(other_actor.node_type == "Actor"):
                        for actor_movies in node.neighbors:
                            for other_actor_movies in other_actor.neighbors:
                                if(actor_movies.name == other_actor_movies.name):
                                    connection_num += 1
                heapq.heappush(pri_queue, (connection_num, node.name))
        pri_queue_len = len(pri_queue)
        for i in range(0, pri_queue_len):
            if (len(pri_queue) > 0):
                elem = heapq.heappop(pri_queue)
                print(elem[1], elem[0])
        print()

    def generate_age_gross_plot(self):
        """
        This function generates a scatter plot of age and gross
        """
        age = []
        gross = []
        for node in self.explored_nodes:
            if(node.node_type == "Actor"):
                age.append(node.value)
                gross.append(node.actor_gross)
        trend = numpy.poly1d(numpy.polyfit(numpy.asarray(age), numpy.asarray(gross), 1))
        plt.plot(age, gross, 'o')
        plt.plot(age, trend(age), '--')
        plt.xlabel("Age")
        plt.ylabel("Gross")
        plt.show()

class Node:
    """
    This is the class that a movie or actor will be with all its information
    """
    def __init__(self, node_type, name, link, parent, neighbors, value):
        """
        Initialization of a node
        :param node_type: given type of node (actor or movie)
        :param name: given name
        :param link: given link (url to wiki page)
        :param parent: given parent (node that found this node)
        :param neighbors: given neighbors (nodes connected to this node in the graph)
        :param value: given value (age or gross)
        """
        self.node_type = node_type
        self.name = name
        self.link = link
        self.parent = parent
        self.neighbors = neighbors
        self.value = value
        self.actor_gross = 0

    def expand_node(self):
        """
        This function gets a wiki page and
        returns the new actor nodes if it is a movie or new movies nodes if it is an actor.
        :return: new nodes to be searched and expanded upon
        """
        # get url and html code
        url = self.link
        r = requests.get(url)
        html_doc = r.text

        # get list of new nodes
        if (self.node_type == "Actor"):
            new_nodes = self.get_new_nodes("Film", "Movie", html_doc, 1)
        else:
            new_nodes = self.get_new_nodes("Cast", "Actor", html_doc, None)
        return new_nodes

    def get_new_nodes(self, id_search, new_node_type, html_doc, link_limit):
        """
        This function is where all the web scraping is.
        The function searches html_doc with BeautifulSoup (BS) and returns the new nodes found.
        :param id_search: what to search for in BS (cast for movies and film for actors)
        :param new_node_type: type of node that children of the current node should be
        :param html_doc: the passed in html document to search
        :param link_limit: 1 if actor and unlimited if movie (due to how pages are set up differently)
        :return: list of new_nodes to search
        """
        new_nodes = []
        soup = BeautifulSoup(html_doc, 'html.parser')
        value = -1.0
        if (id_search == "Cast"):
            # If movie, search for Box office in html_doc
            gross_elem = soup.find(string="Box office")
            if (gross_elem != None):
                value = gross_elem.parent.next_sibling.next_sibling.contents
                if (len(value) > 0 and type(value[0]) == type(gross_elem)):
                    # value[0] is the navigatable string containing the amount of money grossed
                    value = value[0]
                    value = value.split("$")
                    if (len(value) > 0 and value[0] != None):
                        # Split the string so that only the number remains
                        value = value[-1]
                        value = value.split()
                        if (len(value) > 1):
                            if (value[-1] == "million"):
                                # Remove the commas and periods
                                value = value[0].replace(",", "")
                                value = value.replace(".", "") + "00000"
                            else:
                                # Remove the commas
                                value = value[0].replace(",", "")
                                value = value.replace(",", "")
            else:
                logging.warn("Gross for " + self.name + "not found.")

        else:
            # If actor, search for the following class and split the string to get the integer age
            age_elem = soup.find(class_="noprint ForceAgeToShow")
            if (age_elem != None):
                value = age_elem.contents
                if (len(value) > 0):
                    value = value[0]
                    value = value.split()[-1][:-1]
            else:
                logging.warn("Age for " + self.name + " not found.")
        if (type(value) == type("")):
            self.value = int(round(float(value)))
        else:
            self.value = -1

        # Search the html_doc to get the children of the current node and return them so they can be explored later
        film_h3 = soup.find(id=id_search)
        if (film_h3 != None):
            film_h3 = film_h3.parent
            table_h3 = film_h3.next_sibling.next_sibling
            for child in table_h3.children:
                if (type(child) == type(table_h3)):
                    for link in child.find_all("a", limit=link_limit):
                        new_url = link.get("href")
                        if (new_url[0] != "#"):
                            new_url = "https://en.wikipedia.org" + new_url
                            new_node = Node(new_node_type, link.text, new_url, self, [], -1)
                            new_nodes.append(new_node)
        else:
            logging.warn(id_search + " section not found. Unable to add " + self.name + " children to graph.")
        return new_nodes


def generate_graph_from_json():
    """
    A function that creates a graph from a stored JSON file and displays it
    """
    graph = Graph()
    graph.read_from_given_json("data.json")
    graph.generate_visual_graph(1, 20)
    graph.generate_hub_list()
    graph.generate_age_gross_plot()


def generate_graph_from_scraping(num_actors, num_movies):
    """
    A function to generate the graph from webscraping
    :param num_actors: lower bound of number of actors in graph
    :param num_movies: lower bound of number of movies in graph
    """
    # Start logging
    logging.basicConfig(filename='scraper.log', level=logging.INFO)
    # Starting node to begin webscraping
    starting_node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None,
                         [], -1)
    graph = Graph()
    nodes_to_explore = [starting_node]
    # Add nodes to the graph until node requirements are satisfied
    while (graph.num_actor_nodes < num_actors or graph.num_movies_nodes < num_movies):
        if (len(nodes_to_explore) == 0):
            logging.error("Run out of nodes to explore.")
            break

        node = nodes_to_explore.pop(0)
        # Skip movie if enough movies
        if(node.node_type == "Movie" and graph.num_movies_nodes >= num_movies):
            continue
        # Skip actor if enough actors
        if(node.node_type == "Actor" and graph.num_actor_nodes >= num_actors):
            continue
        logging.info("Attempting to add " + node.name + " to graph.")
        nodes_to_explore += graph.attempt_to_add_node(node)
        print("Number of movies in graph:", graph.num_movies_nodes, ",", "Number of actors in graph:",
              graph.num_actor_nodes)

    graph.generate_edges()
    graph.generate_visual_graph(20, 20)
    graph.write_to_json()


def main():
    """
    This is the main function in which we can generate a new graph and write it to a JSON file or we can generate
    the stored graph from the current JSON file.
    """
    generate_graph_from_json()


"""
This is the code that calls the main function.
It is used so that it only runs main if it is the python file being run.
"""
if __name__ == "__main__":
    main()
