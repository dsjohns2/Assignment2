from unittest import TestCase
from scraper import Node
import requests


class TestNode(TestCase):
    def test_expand_node(self):
        node = Node("Actor", "Anna Kendrick", "https://en.wikipedia.org/wiki/Anna_Kendrick", None, [], -1)
        new_nodes = node.expand_node()
        self.assertTrue(new_nodes != None)

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        new_nodes = node.expand_node()
        self.assertTrue(new_nodes != None)

    def test_get_new_nodes(self):
        node = Node("", "", "", None, [], -1)
        r = requests.get("https://en.wikipedia.org/wiki/Anna_Kendrick")
        html_doc = r.text
        new_nodes = node.get_new_nodes("Film", "Movie", html_doc, 1)
        self.assertTrue(new_nodes != None)

        node = Node("", "", "", None, [], -1)
        r = requests.get("https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)")
        html_doc = r.text
        new_nodes = node.get_new_nodes("Cast", "Actor", html_doc, None)
        self.assertTrue(new_nodes != None)