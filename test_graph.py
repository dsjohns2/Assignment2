from unittest import TestCase
from scraper import Graph, Node

class TestGraph(TestCase):
    def test_attempt_to_add_node(self):
        graph = Graph()

        node = Node("Actor", "Anna Kendrick", "https://en.wikipedia.org/wiki/Anna_Kendrick", None, [], -1)
        graph.attempt_to_add_node(node)
        self.assertTrue(len(graph.explored_nodes) == 1)

        graph.attempt_to_add_node(node)
        self.assertTrue(len(graph.explored_nodes) == 1)

        node = Node("Actor", "Bill Murray", "https://en.wikipedia.org/wiki/Bill_Murray", None, [], -1)
        graph.attempt_to_add_node(node)
        self.assertTrue(len(graph.explored_nodes) == 2)

    def test_generate_edges(self):
        graph = Graph()

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        graph.attempt_to_add_node(node)

        node = Node("Actor", "Bill Murray", "https://en.wikipedia.org/wiki/Bill_Murray", None, [], -1)
        graph.attempt_to_add_node(node)

        graph.generate_edges()

    def test_generate_visual_graph(self):
        graph = Graph()

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.generate_visual_graph(0, 1)

    def test_write_to_json(self):
        graph = Graph()

        graph.write_to_json()
        graph.read_from_json("scraper.json")
        self.assertTrue(len(graph.explored_nodes) == 0)

        node = Node("Actor", "Anna Kendrick", "https://en.wikipedia.org/wiki/Anna_Kendrick", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.write_to_json()
        graph.read_from_json("scraper.json")
        self.assertTrue(len(graph.explored_nodes) == 1)

    def test_read_from_given_json(self):
        graph = Graph()

        graph.read_from_given_json("data.json")
        graph.generate_visual_graph(2, 2)

    def test_get_movie_gross(self):
        graph = Graph()

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.get_movie_gross("Fantastic Mr. Fox")

    def test_get_actor_movies(self):
        graph = Graph()

        node = Node("Actor", "Anna Kendrick", "https://en.wikipedia.org/wiki/Anna_Kendrick", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.get_actor_movies("Anna Kendrick")

    def test_get_movie_actors(self):
        graph = Graph()

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.get_movie_actors("Fantastic Mr. Fox")

    def test_get_oldest_actors(self):
        graph = Graph()

        node = Node("Actor", "Anna Kendrick", "https://en.wikipedia.org/wiki/Anna_Kendrick", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.get_oldest_actors(1)

    def test_get_highest_grossing_movies(self):
        graph = Graph()

        node = Node("Movie", "Fantastic Mr. Fox", "https://en.wikipedia.org/wiki/Fantastic_Mr._Fox_(film)", None, [], -1)
        graph.attempt_to_add_node(node)
        graph.get_highest_grossing_movies(1)